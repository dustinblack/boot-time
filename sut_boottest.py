#!/usr/bin/env python3
# Reboots remote systems (SUTs) and captures boot timing results
# Writes JSON file in format ready for ingest in ElasticSearch
#
# Tested on CentOS Stream 8 - Python 3.6.8.
#           Fedora 38       - Python 3.9.18
#

import ast
import sys
import time
import re
import json
import io
import subprocess
import logging
from datetime import datetime
from argparse import ArgumentParser
from typing import Tuple

# grep string that covers the known premutations for when first link is ready
net_str = '-Ei "link.*(ready|up)"'


#####################################
# DICTIONARY Format - dicts initialized in main()
#
# testrun_dict = {
#     "cluster_name": hostname,
#     "date": curtime,
#     "test_type": "boot-time",
#     "sample": 1,
#     "test_config": {
#         testcfg_dict{}
#     },
#     "test_results": {
#         "initramfs":
#             initramfs_dict{}
#         "dlkm":
#             dlkm_dict{}
#         "reboot":
#             reboot_dict{}
#         "satime":
#             satime_dict{}
#         "sablame":
#             sablame_dict{}
#         "earlyservice":
#             earlyservice_dict{}
#         "clktick":
#             clktick_dict{}
#     },
#     "system_config": {
#         syscfg_dict{}
#     }
# }
#


#####################################
# GLOBAL VARS
target = "multi-user.target"  # graphical.target
reboot_timeout = 300  # max. number of seconds: rebootsut()
ssh_timeout = 20  # max. number of seconds: testssh()
retry_int = 2  # client.connect retry interval (in sec)


#####################################
# PARSE ARGUMENTS
parser = ArgumentParser(
    description="Reboots remote systems (SUTs) and captures boot timing results"
)
parser.add_argument(
    "hostname",
    help="Description of device to run boot time tests",
)
parser.add_argument(
    "ip",
    help="IP address of device to run boot time tests",
)
parser.add_argument(
    "username",
    nargs="?",
    default="root",
    help="Username to login to device",
)
parser.add_argument(
    "password",
    nargs="?",
    default="password",
    help="Password to login to device",
)
parser.add_argument(
    "-s",
    "--samples",
    type=int,
    default="1",
    help="Number of samples to collect",
)
parser.add_argument(
    "-b",
    "--blame-count",
    type=int,
    default="10",
    help="Number of services to collect for systemd-analzye blame",
)
parser.add_argument(
    "-r",
    "--raw",
    action="store_true",
    help="Store the raw data",
)
parser.add_argument(
    "--no-reboot",
    action="store_true",
    help="Skip reboot steps and just record data (used only for testing purposes)",
)
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    help="Record verbose timing metrics",
)
parser.add_argument(
    "--min-duration",
    type=int,
    default=0,
    help="Minimum duration in ms filter for verbose output (requires --verbose)",
)
parser.add_argument(
    "--max-duration",
    type=int,
    default=100000,
    help="Maximum duration in ms filter for verbose output (requires --verbose)",
)
parser.add_argument(
    "--max-start-time",
    type=int,
    default=20000,
    help="Maximum start time in ms filter for verbose output (requires --verbose)",
)
parser.add_argument(
    "--kpi-re-pattern",
    type=str,
    default="multi-user.target|sysinit.target|Load Kernel Modules|mounted",
    help="Regular expression to match log messages on for KPIs (requires --verbose)",
)

args = parser.parse_args()

if args.verbose:
    import bokeh_chart


#####################################
# CLASSES
class TimerError(Exception):
    # A custom exception used to report errors in use of Timer class
    # use built-in class for error handling
    pass


class Timer:
    def __init__(self, text="Elapsed time: {:0.2f} seconds", logger=print):
        self._start_time = None
        self.text = text
        self.logger = logger

    def start(self):
        # Start a new timer
        if self._start_time is not None:
            raise TimerError("Timer is running. Use .stop() to stop it")
        self._start_time = time.perf_counter()

    def stop(self):
        # Stop the timer, and report the elapsed time in seconds
        if self._start_time is None:
            raise TimerError("Timer is not running. Use .start() to start it")
        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None
        if self.logger:
            self.logger(self.text.format(elapsed_time))
        return elapsed_time


#####################################
# FUNCTIONS
def write_json(thedict, thefile):
    to_unicode = str
    # Write JSON file
    with io.open(thefile, "w", encoding="utf8") as outfile:
        str_ = json.dumps(
            thedict,
            indent=4,
            sort_keys=False,
            separators=(",", ": "),
            ensure_ascii=False,
        )
        outfile.write(to_unicode(str_))
        outfile.write(to_unicode("\n"))

    print(f"Wrote file: {thefile}")


def init_dict(hname, ip, reboot, ssh, boot_tgt, bl_cnt):
    # Initialize new dict{} for the test config for this workload
    the_dict = {}  # new empty dict

    the_dict["hostname"] = str(hname)
    the_dict["IPaddr"] = str(ip)
    the_dict["reboot_timeout"] = str(reboot)
    the_dict["ssh_timeout"] = str(ssh)
    the_dict["boot_tgt"] = str(boot_tgt)
    the_dict["blame_cnt"] = str(bl_cnt)

    return the_dict


def verify_trim(value):  # Extend to handle str(), float(), int()
    # Verify value. Return value or None if invalid
    if not value:
        ret_val = None
    else:
        ret_val = str(value.strip())

    return ret_val


def testssh(ip, usr, pswd, retry_timeout):
    ssh_status = False  # return value

    # Verify SUT can be ssh'd to
    print(f"> testssh: verifying SSH to {ip}. Timeout: {retry_timeout}s")

    # STOPWATCH returns elapsed time in seconds
    #    et_ssh = Timer(text="testssh: SUT ssh active in {:.2f} seconds",\
    #                      logger=print)
    et_ssh = Timer(text="", logger=None)  # Be silent
    et_ssh.start()

    ssh = run_ssh_cmd("true", ip, usr, pswd, retry_timeout)
    if ssh is None:
        # Error condition - no connection to close
        logging.error(f"testssh: Could not connect to {ip}. Timed out")
        ssh_status = False  # continue on to next SUT
    else:
        # Stop the stopwatch and report on elapsed time
        et_ssh.stop()
        ssh_status = True  # connection success
    return ssh_status


def run_ssh_cmd(
    cmd: str, ip: str, usr: str, passwd, timeout: int = ssh_timeout
) -> Tuple[int, str, str]:
    """
    Run ssh command and returns a Tuple with return code,stdout and stderr
    """

    # Split the command string
    cmd_list = cmd.split()

    ssh_cmd_list = [
        "sshpass",
        "-p",
        passwd,
        "ssh",
        "-o",
        "StrictHostKeyChecking=no",
        f"{usr}@{ip}",
    ]
    ssh_cmd = ssh_cmd_list + cmd_list

    try:
        ssh_proc = subprocess.Popen(
            ssh_cmd,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout_result, stderr_result = ssh_proc.communicate(timeout=timeout)
    except subprocess.CalledProcessError as e:
        logging.error(f"SSH Command Failed: {e}")
        sys.exit(1)
    except subprocess.TimeoutExpired as e:
        logging.error(f"SSH Connection Timeout Expired: {e}")
        sys.exit(1)

    exit_status = ssh_proc.returncode

    return (exit_status, stdout_result, stderr_result)


def remote_systemd(systemd_cmd: str, ssh_ip: str, ssh_user: str):
    systemd_cmd_list = systemd_cmd.split()
    systemd_cmd_list.extend(["-H", f"{ssh_user}@{ssh_ip}"])

    try:
        systemd_proc = subprocess.Popen(
            systemd_cmd_list,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout_result, stderr_result = systemd_proc.communicate(timeout=300)
    except subprocess.CalledProcessError as e:
        logging.error(f"Unable to collect systemd-analyze data: {e}")
        sys.exit(1)
    except subprocess.TimeoutExpired as e:
        logging.error(f"Systemd Connection Timeout Expired: {e}")
        sys.exit(1)

    exit_status = systemd_proc.returncode

    return (exit_status, stdout_result, stderr_result)


def grab_dmesg(ip: str, usr: str, passwd) -> str:
    """
    Grab dmesg logs off the SUT
    """
    (rc, stdout, stderr) = run_ssh_cmd("dmesg -d", ip, usr, passwd)
    if rc:
        logging.error(f"Cannot get dmesg: {stderr}")
        sys.exit(1)
    return stdout


def search_dmesg(queries, dmesg, default_match=True):
    """
    Expects a list of dictionaries with the following format:
    { name_of_entry, regex_text_to_match }
    or if the same text has mulitple matches, the match occurence is specified
    { name_of_entry, (regex_text_to_match, number_to_match) }

    Queries are already in a match `()`
    """
    matches = []

    for query in queries:
        name = next(iter(query))
        value = next(iter(query.values()))
        # Optional value to specify which match to use in a tuple, otherwise use 0
        if isinstance(value, tuple):
            entry = value[0]
            index = value[1]
        else:
            entry = value
            index = 0

        # By default, set the entry to a group
        if default_match:
            entry = "(" + entry + ")"
        # Match the timestamp as a group
        regex_query = rf"^\[\s*([\d.]+).*\]\s+{entry}"
        result = re.findall(regex_query, dmesg, re.MULTILINE)

        if not result:
            raise ValueError(f'"{entry}" was not found in dmesg.')

        matches.append((name,) + result[index])
    return matches


def calc_duration(msgs):
    previous_msg = None
    output = {}
    for msg in msgs:
        # Create timestamp entries
        (name, timestamp, *_) = msg
        field_name = name + "_ts"
        output[field_name] = float(timestamp)

        # Create duration entries
        if previous_msg:
            (prev_name, prev_timestamp, *_) = previous_msg
            duration = float(timestamp) - float(prev_timestamp)
            field_name = f"{prev_name}_{name}_int"
            output[field_name] = duration

        previous_msg = msg
    return output


def initramfs(dmesg: str):
    queries = [
        {"unpack": "Trying to unpack rootfs image as initramfs|Unpacking initramfs"},
        {"init": ".* as init process"},
        {"systemd": ".* running in system mode"},
    ]

    msgs = search_dmesg(queries, dmesg)
    return calc_duration(msgs)


def dlkm(dmesg: str):
    queries = [
        {"systemd": ".* running in system mode"},
        {"udev": ".*Listening on udev Control Socket"},
        {"start_kmod_load": ".*Starting Load Kernel Modules"},
        {"finish_kmod_load": ".*Finished Load Kernel Modules"},
    ]

    msgs = search_dmesg(queries, dmesg)
    return calc_duration(msgs)


def early_service(dmesg: str):
    queries = [
        {"systemd": ".* running in system mode"},
        {"earlyservice": ".*early service"},
    ]

    msgs = search_dmesg(queries, dmesg)
    return calc_duration(msgs)


def clk_ticks(dmesg: str):
    entry_query = r": (\d+)"
    queries = [
        {"primary_entry": rf".*primary_entry\(\){entry_query}"},
        {"time_init": rf".*time_init\(\){entry_query}"},
        {"setup_arch": rf".*setup_arch\(\){entry_query}"},
    ]
    matches = search_dmesg(queries, dmesg, False)

    clk_tick = {}
    for match in matches:
        name, _, tick = match
        seconds = float(tick) / 19200000
        clk_tick[name + "_ts"] = seconds

    # Calculate interval
    clk_tick["primary_entry_time_init_int"] = (
        clk_tick["time_init_ts"] - clk_tick["primary_entry_ts"]
    )
    clk_tick["primary_entry_setup_arch_int"] = (
        clk_tick["setup_arch_ts"] - clk_tick["primary_entry_ts"]
    )
    return clk_tick


def verbose_filter(
    log_item: dict,
    min_duration: int = args.min_duration,
    max_duration: int = args.max_duration,
    max_start_time: int = args.max_start_time,
    kpi_re_pattern: str = args.kpi_re_pattern,
):
    if (
        min_duration <= log_item["time"] / 1000 <= max_duration
        and log_item["activating"] / 1000 < max_start_time
    ) or re.search(kpi_re_pattern, log_item["name"]):
        if re.search(kpi_re_pattern, log_item["name"]):
            log_item["kpi"] = True
        return log_item
    else:
        exit


def dmesg_to_list(raw_dmesg):
    dmesg_lines = raw_dmesg.splitlines()

    dmesg_list = []

    for line in dmesg_lines:
        log = re.split("] | <", line, maxsplit=2)
        dmesg_dict = {}
        dmesg_dict["name"] = log[2]
        dmesg_dict["activating"] = int(float(log[0].strip("[ ")) * 1000000)
        dmesg_dict["time"] = int(float(log[1].strip(">")) * 1000000)
        filter_check = verbose_filter(dmesg_dict)
        if filter_check:
            dmesg_list.append(filter_check)

    return dmesg_list


##################################
# PARSER Functions
def parse_osrelease(cmd_out, the_dict):
    # PRETTY NAME value
    for line in cmd_out.split("\n"):
        if "PRETTY_NAME=" in line:
            raw_str = re.search("PRETTY_NAME=(.*)", cmd_out).group(1)
            pname = raw_str.replace('"', "")  # remove surrounding quotes
    the_dict["osrelease"] = verify_trim(pname)

    return the_dict


def parse_lscpu(cmd_out, the_dict):
    # cpu architecture
    for line in cmd_out.split("\n"):
        if "Architecture:" in line:
            arch = re.search("Architecture:(.*)", cmd_out).group(1)
    the_dict["architecture"] = verify_trim(arch)

    # cpu model
    for line in cmd_out.split("\n"):
        if "Model name:" in line:
            model = re.search("Model name.*:(.*)", cmd_out).group(1)
    the_dict["model"] = verify_trim(model)

    # Number of cores
    for line in cmd_out.split("\n"):
        if "CPU(s):" in line:
            numcores = re.search("CPU\\(s\\):(.*)", cmd_out).group(1)
    the_dict["numcores"] = verify_trim(numcores)

    # BogoMIPS
    for line in cmd_out.split("\n"):
        if "BogoMIPS:" in line:
            bogo = re.search("BogoMIPS:(.*)", cmd_out).group(1)
    the_dict["bogomips"] = verify_trim(bogo)

    return the_dict


def parse_satime(cmd_out, the_dict):
    # 'systemd-analyze time' key metrics and key names
    satime_list = ["kernel", "initrd", "userspace"]
    satotal = float(0.0)

    # Results can in seconds or millisec, so search for both
    for regex in satime_list:
        match_sec = re.findall("(\\d+\\.\\d+)s\\s\\(" + regex + "\\)", cmd_out)
        match_ms = re.findall("(\\d+)ms\\s\\(" + regex + "\\)", cmd_out)
        if match_sec:
            the_dict[regex] = float(match_sec[0])
            satotal = satotal + float(match_sec[0])
        elif match_ms:
            ms = float(match_ms[0]) / 1000
            the_dict[regex] = float(ms)
            satotal = satotal + float(ms)
        else:
            the_dict[regex] = float(0.0)

    # add TOTAL time to the_dict[]
    the_dict["total"] = float(satotal)

    return the_dict


def parse_sablame(cmd_out, the_dict, blame_cnt):
    # Parse cmd output, calc time in seconds and populate dict
    cntr = 1
    for line in cmd_out.split("\n"):
        if cntr <= int(blame_cnt):
            words = line.split()
            service = words[-1]
            minutes = re.search("(\\d+)min", line)
            seconds = re.search("(\\d+\\.\\d+)s", line)
            millisec = re.search("(\\d+)ms", line)
            if minutes and seconds:
                min = minutes[0].strip("min")
                sec = seconds[0].strip("s")
                total_sec = str((int(min) * 60) + float(sec))
            elif seconds and not minutes:
                total_sec = seconds[0].strip("s")
            elif millisec:
                ms = millisec[0].strip("ms")
                total_sec = str((int(ms) / 1000) % 60)

            if service and total_sec:
                cntr += 1
                the_dict[service] = float(total_sec)
        else:
            break

    return the_dict


##########
# Per-Phase functions
# Phase 1 - gather sysfacts and populate dict{}
def phase1(sship, sshuser, sshpasswd):
    ph1_dict = {}  # empty dict{} for us in this phase function
    #              KEY        COMMAND
    cmd_list = [
        ("kernel", "uname -r"),
        ("osrelease", "cat /etc/os-release | grep PRETTY_NAME"),
        ("various", "lscpu"),
    ]

    for x, (key, cmd_str) in enumerate(cmd_list):
        _, stdout, _ = run_ssh_cmd(cmd_str, sship, sshuser, sshpasswd)

        # Populate dict{}, format varies with command type
        if "lscpu" in cmd_str:
            # Parse results from 'lscpu' command
            ph1_dict = parse_lscpu(stdout, ph1_dict)
        elif "os-release" in cmd_str:
            # Parse results from 'cat' command
            ph1_dict = parse_osrelease(stdout, ph1_dict)
        else:
            ph1_dict[key] = str(verify_trim(stdout))

    return ph1_dict


# Phase 2 - reboot and wait for system readiness
def phase2(ip, usr, passwd):
    ph3_dict = {}  # empty dict{} for use in this phase function

    # Issue reboot cmd
    _, stdout, _ = run_ssh_cmd("reboot", ip, usr, passwd)

    ######################
    # START the clock on total shutdown and reboot time
    et_reboot = Timer(
        text="phase2: SUT shutdown and reboot required {:.2f} seconds", logger=print
    )  # SILENCE THIS 'logger=none'
    et_reboot.start()

    # Need to stall/pause while shutdown completes
    delay = 30  # just a guess, pause for reboot to complete
    time.sleep(delay)  # just a guess...

    # Start ssh timer
    ssh_start = time.time()

    # Verify SSH responds. Wait upto 'reboot_timeout' (GLOBAL)
    ping_ssh = testssh(ip, usr, passwd, reboot_timeout)
    if ping_ssh is False:
        logging.error("phase2: Aborting test run")
        sys.exit(1)

    # Stop ssh timer and calc elapsed time
    ssh_et = time.time() - ssh_start

    # SSH is active, now wait for system up condition:
    #     'systemctl list-jobs == No jobs running'
    sysctl_start = time.time()
    rebooted = False
    pause = 1  # NOTE: timer granularity NEEDS WORK

    print(
        f"phase2: SUT {ip}, ", f"waiting {reboot_timeout}s for reboot to complete..." ""
    )
    while rebooted is False:
        # if cmd==True, then SUT has completed boot process
        cmd_str = "systemctl list-jobs | grep -q 'No jobs running'"
        exit_status, stdout, _ = run_ssh_cmd(cmd_str, ip, usr, passwd)
        # Block on completion of exec_command

        # Test for completed boot
        if exit_status == 0:
            rebooted = True  # Triggers break out of loop
        else:
            time.sleep(pause)

        # Test if exceeded time limit
        sysctl_et = time.time() - sysctl_start
        total_et = ssh_et + sysctl_et
        if total_et >= reboot_timeout:
            # Error, suggested to explictly close
            logging.error(f"rebootsut: SUT {ip} reboot Timed out")
            sys.exit(1)

    # SUCCESS
    # Stop timer and Populate reboot_dict{} with results
    reboot_et = et_reboot.stop()
    ph3_dict["reboot_et"] = float(reboot_et)
    ph3_dict["ssh_et"] = float(ssh_et)
    ph3_dict["sysctl_et"] = float(sysctl_et)
    ph3_dict["total_et"] = float(total_et)

    # Record 'link_is_up' timestamp value from dmesg buffer
    # - search for 'net_str' as defined in SUT_VARS section
    # EXAMPLE messages:
    # [   15.744369] atlantic 0002:81:00.0 eth0: atlantic: link change...
    # [   15.746078] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
    # dmesg | grep -m 1 eth0 | cut -d "[" -f2 | cut -d "]" -f1
    cmd_linkup = "dmesg | grep -m 1 {}".format(net_str)

    exit_status, stdout, _ = run_ssh_cmd(cmd_linkup, ip, usr, passwd)
    # Block on completion of exec_command

    if exit_status:
        logging.error("Unable to find first link is up string. Check `net_str`.")
        sys.exit(1)
    cmdres_linkup = stdout
    pattern = r"\[\s*([\d.]+)\]"
    linkup_timestamp = re.findall(pattern, cmdres_linkup)
    # Test for valid value
    ph3_dict["link_is_up"] = float(linkup_timestamp[0])
    # Set to 0.0 if value is invalid
    ph3_dict["link_is_up"] = float(0.0)

    # Record boot target
    cmd_bt = "systemctl get-defaults"
    _, stdout, _ = run_ssh_cmd(cmd_bt, ip, usr, passwd)
    # Block on completion of exec_command
    cmdres_bt = stdout
    ph3_dict["boot_tgt"] = str(verify_trim(cmdres_bt))

    return ph3_dict


# Phase 3
# - executes instr_list cmds, builds dict from cmd result and returns dict
def phase3(ip, usr, passwd, num_blames):
    satime_dict = {}  # systemd-analyze time results
    sablame_dict = {}  # systemd-analyze blame results
    ph4_dict = {}  # systemd-analyze complete results
    raw_dict = {}  # diecitonary of raw results of comds
    #                 KEY        COMMAND
    instr_list = [
        ("sa_time", "systemd-analyze time"),
        ("sa_blame", "systemd-analyze blame --no-pager | grep service"),
    ]

    # The plot output will go into the verbose output object, not into ph4_dict
    saplot_timings = []  # systemd-analyze plot results
    if args.verbose:
        instr_list.append(("sa_plot", "systemd-analyze plot --json=short --no-legend"))

    for x, (key, cmd_str) in enumerate(instr_list):
        # FIXME -- Systemd version >=253 is currently needed for json output of the
        # plot,so we run this with the local systemd (253+) using it's built-in remote
        # execution option. This is much slower that a remote ssh run. A backport
        # request for the json output feature for RHEL 9 is pending as of this writing.
        # NOTE -- Currently requires working key-based ssh auth with the remote host.
        if args.verbose and key == "sa_plot":
            _, stdout, _ = remote_systemd(cmd_str, ip, usr)
        else:
            _, stdout, _ = run_ssh_cmd(cmd_str, ip, usr, passwd)

        # Store the output as raw results for debug purposes
        raw_dict[key] = stdout

        # populate dictionaries, format varies with command type
        if "sa_time" in key:
            satime_dict = parse_satime(stdout, satime_dict)
            ph4_dict[key] = satime_dict

        if "sa_blame" in key:
            sablame_dict = parse_sablame(stdout, sablame_dict, num_blames)
            ph4_dict[key] = sablame_dict
            raw_dict[key] = stdout

        if "sa_plot" in key:
            saplot_dict = ast.literal_eval(stdout)
            for log in saplot_dict:
                filter_check = verbose_filter(log)
                if filter_check:
                    saplot_timings.append(filter_check)

    return ph4_dict, raw_dict, saplot_timings


##############################################################
# MAIN


def main():
    # Parse CLI args and assign them to their respective variables
    (sut_host, sut_ip, sut_usr, sut_pswd) = (
        args.hostname,
        args.ip,
        args.username,
        args.password,
    )
    run_count = args.samples
    blame_cnt = args.blame_count
    save_raw = args.raw

    # Dependency check for verbose output
    if args.verbose:
        # Verbose output currently requires a local systemd-analyze command of version
        # 253 or higher and key-based SSH authentication to the SUT
        try:
            systemd_analyze = subprocess.Popen(
                ["systemd-analyze", "--version"], stdout=subprocess.PIPE, text=True
            )
            grep_systemd = subprocess.Popen(
                ["grep", "^systemd"],
                stdin=systemd_analyze.stdout,
                stdout=subprocess.PIPE,
                text=True,
            )
            systemd_vers = subprocess.Popen(
                ["awk", "{print $2}"],
                stdin=grep_systemd.stdout,
                stdout=subprocess.PIPE,
                text=True,
            ).communicate()[0]
        except subprocess.CalledProcessError as e:
            logging.error(
                f"Unable to check local systemd-analyze version; >=253 is required: {e}"
            )
            sys.exit(1)
        if int(systemd_vers) < 253:
            logging.error(
                f"""
                Verbose output requires systemd-analyze version >=253;
                You have {systemd_vers}
                """
            )
            sys.exit(1)
        try:
            ssh_check = subprocess.call(
                [
                    "ssh",
                    "-o",
                    "PasswordAuthentication=False",
                    f"{args.username}@{args.ip}",
                    "true",
                ],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
        except subprocess.CalledProcessError as e:
            logging.error(
                f"Verbose output requires key-based SSH authentication to the SUT: {e}"
            )
            sys.exit(1)
        if ssh_check:
            logging.error(
                "Verbose output requires key-based SSH authentication to the SUT."
            )
            sys.exit(1)

    ##########################
    # OUTER LOOP - For each SUT
    # initialize vars and print msg for this SUT being tested
    print(f"\n***SUT: {sut_ip}  {sut_host}  Number of Runs: {run_count}***")
    results_list = []  # comprehensive results - list of dicts
    raw_results_list = []  # Raw output from the commands executed
    outfilename = str(sut_host + "_" + datetime.now().strftime("%m_%d_%Y_%H_%M_%S"))
    run_number = 1  # initialize
    while run_number <= run_count:
        print(f"\n** Run: {run_number} **")

        # Dictionaries
        testrun_dict = {}  # complete testrun results (per SUT)
        testcfg_dict = {}  # test configuration
        syscfg_dict = {}  # system configuration
        data_dict = {}  # testdata results (nested)
        raw_dict = {}  # raw logs from commands

        # Verify connectivity to SUT
        ping_ssh1 = testssh(sut_ip, sut_usr, sut_pswd, ssh_timeout)
        if ping_ssh1 is False:
            continue  # skip this SUT

        # Add to dict{} for this SUT
        testrun_dict["cluster_name"] = str(sut_host)
        curtime = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        testrun_dict["date"] = str(curtime.strip())
        testrun_dict["test_type"] = "boot-time"  # hardcoded
        testrun_dict["sample"] = int(run_number)

        # Initialize testcfg dict{} for this SUT
        testcfg_dict = init_dict(
            sut_host, sut_ip, reboot_timeout, ssh_timeout, target, blame_cnt
        )
        testrun_dict["test_config"] = testcfg_dict

        ############
        # INNER LOOP
        # Proceed through testing phases
        # ----------
        # Phase 1: gather system facts
        print("*Phase 1 - gather facts")
        syscfg_dict = phase1(sut_ip, sut_usr, sut_pswd)

        if args.no_reboot:
            print("*Skipping reboot phase")
        else:
            # ----------
            # Phase 2: initiate reboot, wait for system readiness
            #          and record timing results into reboot_dict{}
            print("*Phase 2 - initiate reboot and wait for system readiness")
            reboot_dict = phase2(sut_ip, sut_usr, sut_pswd)
            # Verify connectivity to freshly rebooted SUT
            ping_ssh2 = testssh(sut_ip, sut_usr, sut_pswd, ssh_timeout)
            if ping_ssh2 is False:
                continue  # reboot failed, abort this SUT test

        # ----------
        # Phase 3: instrument SUT reboot w/systemd-analyze commands
        # NOTE: sa_dict is nested with "sa_time" and "sa_blame" keys
        print("*Phase 3 - record reboot timing stats")
        sa_dict, raw_dict, saplot_timings = phase3(sut_ip, sut_usr, sut_pswd, blame_cnt)

        # ----------
        # Phases that rely on dmesg
        dmesg = grab_dmesg(sut_ip, sut_usr, sut_pswd)
        raw_dict["dmesg"] = dmesg

        dmesg_phases = [
            ("initramfs", initramfs),
            ("earlyservice", early_service),
            ("dlkm", dlkm),
            ("clktick", clk_ticks),
        ]

        phase_num = 4
        dmesg_dict = {}
        for phase in dmesg_phases:
            name, phase_function = phase
            print(f"*Phase {phase_num} - {name} timing stats")
            try:
                results = phase_function(dmesg)
                data_dict.update({name: results})
            except Exception as e:
                logging.warning(f"{name} skipped...{e}")
                dmesg_dict.update({name: {}})
            phase_num += 1

        # Sanity check: Make sure these two are always equal since it is the same
        assert dmesg_dict.get("dlkm", {}).get("systemd_ts") == dmesg_dict.get(
            "initramfs", {}
        ).get("systemd_ts"), "systemd_ts do not match for dlkm and initramf"

        ######################
        # All PHASEs for this SUT completed
        # Insert existing test results into 'test_results' section
        try:
            data_dict["reboot"] = reboot_dict
        except UnboundLocalError:
            data_dict["reboot"] = "*Reboot Skipped*"
        data_dict["satime"] = sa_dict["sa_time"]
        data_dict["sablame"] = sa_dict["sa_blame"]
        data_dict.update(dmesg_dict)

        # Insert complete data_dict{} into testrun_dict (final dictionary)
        testrun_dict["test_results"] = data_dict

        # Insert syscfg_dict{} into testrun_dict{}
        testrun_dict["system_config"] = syscfg_dict

        if args.verbose:
            dmesg_timings = dmesg_to_list(dmesg)
            if saplot_timings:
                timings = dmesg_timings + saplot_timings
            else:
                timings = dmesg_timings

            # Insert verbose timing details
            testrun_dict["timing_details"] = timings

            # Build the Bokeh chart
            print(
                bokeh_chart.build_chart(
                    timings,
                    testrun_dict["date"],
                    testrun_dict["test_config"],
                    testrun_dict["system_config"],
                    outfilename,
                    run_number,
                )
            )

        # Insert test results for this SUT into results_list[]
        results_list.append(testrun_dict)

        # Save the raw results if enabled
        if save_raw:
            raw_results_list.append(raw_dict)

        run_number += 1  # incr run cntr

    print(f"\n+++TESTING for {sut_host} COMPLETED+++")

    write_json(results_list, outfilename + ".json")

    if save_raw:
        write_json(raw_results_list, outfilename + "_raw.json")

    print("+++TESTING for all systems COMPLETED+++")


# END MAIN

if __name__ == "__main__":
    main()

##############################################################
