# BootTime
Collection of scripts useful to investigate linux boot-time and a CI pipeline to execute them

## Pipeline

The GitLab CI pipeline excutes an end to end test on hardware specified by IP address.


### Pipeline options

To execute pipeline, go to [Build > Pipeliens > Run pipelines](https://gitlab.com/redhat/edge/tests/perfscale/boot-time/-/pipelines/new).

By default, the pipeline executes the boot-time test on `SUT_IP` and assumes the hardware is all setup accordingly.

The following variables add ability to flash a new kernel on the System Under Test (SUT). Currently only
supports Qualcomm QDrive3 and Ride4:
- `KERNEL_ABOOT_PATH`: Path to an aboot file on the SUT to flash.
- `KERNEL_RPM_PATH`: Path to a folder on the SUT that has kernel RPMS to install.
- `KERNEL_RPM_DOWNLOAD`: A web URL that contains kernel RPMs to download. Must be able to list the directory contents.

## Scripts

### Pre-req
#### Setup
Uses `pipenv` for dependencies.
```
cd boot-time
pip3 install pipenv
pipenv install
```

#### Execute
To run any boot-time script, run:
```
pipenv shell
```

### Descriptions
sut_boottest.py : latest JSON prototype (sut_boottestREADME.pdf: documents automation flow)
* Reboots remote systems (SUTs) and captures boot timing results
* Writes JSON file in format ready for ingest in ElasticSearch, see 'example.json'
* The `-v | --verbose` flag collects all dmesg and user-space systemd logs and generates an interactive chart for each sample.

*Note: Verbose output currently requires a local `systemd-analyze` command version >=253 and passwordless SSH connectivity to the SUT*

calcstats_json.py : post processes sut_boottest.py JSON file
* calculates run-to-run variance stats for:
* systemd-analyze by parsing 'sa_time' JSON object
* systemd-analyze by parsing 'sa_blame' JSON object
* DMESG
    * 'link is up' message and timestamp
    * initramfs
    * early service
    * early clock ticks

gsheets.py: Takes the JSON output of calcstats_json.py and sut_boottest.py and create a
Google Sheet

systemd_unitfile/
Assists with measurement of custom systemd-unitfile timings
* see README.md in directory

Sandbox/
Collection of scripts created during development of Boot-time automation
* reboot_test.py: instruments reboot of remote system under test
* ex_channel.py: paramiko ssh example
* rcv_exit_status.py: illustrates blocking with paramiko exec cmds
* sa_json.py : produces json from both 'time' and 'blame' results
NOTE: requires 'pip3.6 import distro'

VMs/
Two scripts which work together to report on boot-time stats for a VM
* start_vm_boot.sh: execute on HOST. You need to add cmdline to start your VM
* print_boottime.sh: execute in VM, edit $HOSTNAME. Prints boot-time metrics (see sample.txt)
* sample.txt: example of output from 'print_boottimek.sh'
