#!/usr/bin/env python3
import sys
import argparse
import json
import webbrowser
from pathlib import Path
import gspread
from googleapiclient.discovery import build
from google.oauth2 import service_account
from google.oauth2 import credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


def parse_args():
    parser = argparse.ArgumentParser(
            description='Generate a gsheet for boot-time results'
            )

    parser.add_argument(
            'stat_json_file',
            help='Output from calcstats_json.py')

    parser.add_argument(
            'raw_data_json_file',
            help='Output from sut_boottest.py')

    parser.add_argument(
            'email',
            nargs='?',
            help='E-mail address to share gsheet')

    parser.add_argument(
            'cred_path',
            nargs='?',
            default="credentials.json",
            help='Path to credentials for Google API'),

    parser.add_argument(
            '-s',
            '--service-account',
            action="store_true",
            help='Set credential type to service account, otherwise defaults to oauth.',
            required=False,
            )
    return parser.parse_args()

def transfer_ownership(gsheet,email):
    drive_creds = service_account.Credentials.from_service_account_file(filename='credentials.json', scopes=['https://www.googleapis.com/auth/drive'])
    drive_service = build('drive', 'v3', credentials=drive_creds)
    transfer_body = {
        "role": "owner",
        "transferOwnership": True,
        "type": "user",
        "emailAddress": email,
    }
    response = drive_service.permissions().create(
            fileId=gsheet.id,
            body=transfer_body,
            transferOwnership=True,
            supportsAllDrives=True,
            ).execute()


def generate_sheet(all_stats, worksheet, header, system_config, show_headers=True):
    for key, stats in all_stats.items():
        if not stats:
            continue

        values = [[stat[key] for key in header] for stat in stats]

        # Create row
        row = [[key.upper()]] 
        if show_headers:
            row += [header]
        row += values

        worksheet.insert_rows(row, 1)
        # last_col = chr(ord('A') + len(header)-1)
        # worksheet.merge_cells(f"A1:{last_col}1")
    worksheet.insert_row([], 1)
    worksheet.insert_rows(system_config, 1)

def google_authenticate(scopes, cred_path, is_service_account = False):
    # Use service account for CI
    if is_service_account:
        creds = service_account.Credentials.from_service_account_file(filename=cred_path, scopes=scopes)
    # Otherwise oauth
    else:
        authorized_user_file_path = 'boot-time_token.json'
        # Check if this was previous authoriazed
        if Path(authorized_user_file_path).exists():
            creds = credentials.Credentials.from_authorized_user_file(authorized_user_file_path)
            if creds.expired:
                print("Refreshing token...")
                creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(cred_path, scopes)
            creds = flow.run_local_server(port=0)
            Path(authorized_user_file_path).write_text(creds.to_json())

    return gspread.authorize(creds)


def main(argv):
    if len(argv) < 3:
        print(argv[0] + ' json_file_from_calc_stats json_file_from_sutboottest email')
        sys.exit(1)

    # Parse args
    args = parse_args()
    stat_json_file = args.stat_json_file
    raw_data_json_file = args.raw_data_json_file
    email = args.email
    cred_path = args.cred_path
    is_service_account = args.service_account


    all_stats = json.loads(Path(stat_json_file).read_text('UTF-8'))

    # Assumes the results from sut_boottest.py is all from one system
    raw_data = json.loads(Path(raw_data_json_file).read_text('UTF-8'))
    cluster_name = raw_data[0]['cluster_name']
    system_config = raw_data[0]['system_config']
    system_config_list = [[key, system_config[key]] for key in system_config.keys()]

    scopes = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
    gcreds = google_authenticate(scopes, cred_path, is_service_account)

    print("Successfully authorized...")
    
    # Shared folder setup with P&S team that has access
    gshared_folder_id = "1OcwOQw2YXBlX2PHr0INkO9l7GUbW9ldK"

    print("Creating gsheet...")
    gsheet = gcreds.create(
            'Automotive Boot Time Test - ' + stat_json_file.rstrip('.json'),
            gshared_folder_id)

    try:

        # Make sure people get access, mainly useful when using service account
        if is_service_account:
            gsheet.share('acalhoun-directs@redhat.com', perm_type='group', role='writer', notify=False,)
            if email: 
                gsheet.share(email, perm_type='user', role='writer')

        # Convert the output from calc_stat.spy into the first sheet
        worksheet = gsheet.get_worksheet(0)
        worksheet.update_title(f'Boot time {cluster_name} - Raw')
        generate_sheet(all_stats,
                       worksheet,
                       ['name', 'samples', 'mean', 'std_dev', 'percent_sd'],
                       system_config_list
                       )

        # Create a sheet that only has the mean values. Optimized for copy/pasting in reports
        generate_sheet(all_stats,
                       gsheet.add_worksheet(title=f'Boot time {cluster_name} - Mean only', rows=0, cols=0),
                       ['name',  'mean'],
                       system_config_list,
                       show_headers=False
                       )

        webbrowser.open_new_tab(gsheet.url)

        print(f'URL: {gsheet.url}')

    except Exception as e:
        gcreds.del_spreadsheet(gsheet.id)
        raise e

if __name__ == '__main__':
    main(sys.argv)
